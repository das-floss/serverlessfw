FROM curlimages/curl:8.11.0 as downloader

# set to a writeable directory:
WORKDIR /home/curl_user/awscliv2

# Download awscliv2
RUN curl --location --fail --remote-name https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip && unzip awscli-exe-linux-x86_64.zip


FROM --platform=linux/amd64 node:22.11-bookworm-slim
ARG SERVERLESS_FW_VERSION

# GitLab's AutoDevOps does not have multiarch support atm. See https://gitlab.com/gitlab-org/gitlab/-/issues/214552
ENV TARGETARCH=amd64

# install serverlessFW
RUN npm config set prefix /usr/local && npm install -g serverless@${SERVERLESS_FW_VERSION} && npm cache clean --force

# install awscliv2
COPY --from=downloader /home/curl_user/awscliv2/aws /tmp/awscliv2
RUN /tmp/awscliv2/install && rm -rf /tmp/awscliv2
